public class assignment1 {
    //main method
    public static void main(String[] args) {
        //declare and initialize gender
        String gender = "laki-laki";
        //declare and initialize nama
        String nama = "Peter";
        //declare and initialize umur
        int umur = 17;
        //declare and initialize rata-rata nilai
        float rata2Nilai = 8.5F;

        //print sentences
        System.out.println("Seorang siswa " + gender + " bernama " + nama + "," + " berumur " + umur + " tahun, "+ "memiliki nilai rata-rata " + rata2Nilai +".");

    }
}
