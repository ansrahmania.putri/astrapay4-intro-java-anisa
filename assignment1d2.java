import java.util.Scanner;

public class assignment1d2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Nama: ");
        String inputNama = input.nextLine();
        System.out.print("Tanggal lahir: ");
        int inputTanggal = input.nextInt();
        System.out.print("Bulan lahir: ");
        int inputBulan = input.nextInt();
        System.out.print("Tahun lahir: ");
        int inputTahun = input.nextInt();

        String bulan = "";

        if (inputBulan == 1) {
            bulan = "Januari";
        } else if (inputBulan == 2) {
            bulan = "Februari";
        } else if (inputBulan == 3) {
            bulan = "Maret";
        } else if (inputBulan == 4) {
            bulan = "April";
        } else if (inputBulan == 5) {
            bulan = "Mei";
        } else if (inputBulan == 6) {
            bulan = "Juni";
        } else if (inputBulan == 7) {
            bulan = "Juli";
        } else if (inputBulan == 8) {
            bulan = "Agustus";
        } else if (inputBulan == 9) {
            bulan = "September";
        } else if (inputBulan == 10) {
            bulan = "Oktober";
        } else if (inputBulan == 11) {
            bulan = "November";
        } else if (inputBulan == 12) {
            bulan = "Desember";
        }

        int umur = 2022 - inputTahun;
        System.out.println("Nama saya " + inputNama + ", lahir " + inputTanggal+ " "+ bulan + " " + inputTahun +" "+ " berumur" + " "+ umur + " tahun.");

        input.close();

    }
}
