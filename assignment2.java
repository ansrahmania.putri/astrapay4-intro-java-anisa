public class assignment2 {
    //main method
    public static void main(String[] args) {
        //declare and initialize boolean
        boolean nilaiBoolean = true;
        //declare and initialize byte
        byte nilaiByte = 120;
        //declare and initialize short
        short nilaiShort = 240;
        //declare and initialize integer
        int nilaiInteger = 2000;
        //declare and initialize long
        long nilaiLong = Long.valueOf(10000);
        //declare and initialize double
        double nilaiDouble = 64.64;
        //declare and initialize float
        float nilaiFloat = 16.16F;
        //declare and initialize char
        char nilaiChar = 'z';
        //declare and initialize string
        String nilaiString = "G2Academy";

        //print boolean
        System.out.println("Boolean");
        //print boolean value
        System.out.println(nilaiBoolean);
        //print byte
        System.out.println("Byte");
        //print byte value
        System.out.println(nilaiByte);
        //print short
        System.out.println("Short");
        //print short value
        System.out.println(nilaiShort);
        //print integer
        System.out.println("Integer");
        //print integer value
        System.out.println(nilaiInteger);
        //print long
        System.out.println("Long");
        //print long value
        System.out.println(nilaiLong);
        //print double
        System.out.println("Double");
        //print nilai double
        System.out.println(nilaiDouble);
        //print float
        System.out.println("Float");
        //print nilai float
        System.out.println(nilaiFloat);
        //print character
        System.out.println("Character");
        //print character value
        System.out.println(nilaiChar);
        //print string
        System.out.println("String");
        //print string value
        System.out.println(nilaiString);

    }
}
