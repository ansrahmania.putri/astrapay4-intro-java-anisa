import java.util.Scanner;

public class assignment2d2 {
    public static int penjumlahan (int x, int y) {
        return x + y;
    }

    public static int pengurangan (int x, int y) {
        return x - y;
    }

    public static int perkalian (int x, int y) {
        return x * y;
    }

    public static int pembagian (int x, int y) {
        return x / y;
    }

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int firstNumber, secondNumber, choices, result = 0;

        System.out.println("1. Penjumlahan");
        System.out.println("2. Pengurangan");
        System.out.println("3. Perkalian");
        System.out.println("4. Pembagian");

        System.out.println("Choose an operator (1/2/3/4)");
        choices = input.nextInt();

        System.out.println("Enter first number: ");
        firstNumber = input.nextInt();
        System.out.println("Enter second number: ");
        secondNumber = input.nextInt();

        switch (choices) {
            case 1 :
                result = penjumlahan(firstNumber, secondNumber);
                break;
            case 2 :
                result = pengurangan(firstNumber, secondNumber);
                break;
            case 3 :
                result = perkalian(firstNumber, secondNumber);
                break;
            case 4 :
                result = pembagian(firstNumber, secondNumber);
                break;
            default :
                System.out.println("Operator you enter is not valid");
        }

        if (choices > 0 && choices <=4) {
            System.out.println("Results is " + result);

        }

        input.close();
    }
}
