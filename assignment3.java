public class assignment3 {
    //state main method
    public static void main(String[] args) {
        //declare and initialize panjang persegi panjang
        int panjang = 12;
        //declare and initialize lebar persegi panjang
        int lebar = 8;
        //declare and initialize calculation of luas persegi panjang
        int luasPersegi = panjang * lebar;

        //print luas persegi
        System.out.println(luasPersegi);

        //declare and initialize alas segitiga
        float alas = 7.0f;
        //declare and initialize tinggi segitiga
        float tinggi = 3.5f;
        //declare and initialize calculation of luas segitiga
        float luasSegitiga = (alas * tinggi)/2;

        //print luas segitiga
        System.out.println(luasSegitiga);

        //check if luas persegi > luas segitiga
        boolean pengecekan = luasPersegi > luasSegitiga ? true: false;
        //print pengecekan result
        System.out.println(pengecekan);

        //decrement 3 times
        luasPersegi--;
        luasPersegi--;
        luasPersegi--;
        //print luas persegi after decrement
        System.out.println(luasPersegi);
        //increment 6 times
        luasPersegi++;
        luasPersegi++;
        luasPersegi++;
        luasPersegi++;
        luasPersegi++;
        luasPersegi++;
        //print luas persegi after increment
        System.out.println(luasPersegi);


    }

}
