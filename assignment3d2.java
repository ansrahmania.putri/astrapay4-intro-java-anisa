import java.util.Scanner;

public class assignment3d2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Nama: ");
        String inputNama = input.nextLine();
        System.out.print("Tanggal lahir: ");
        int inputTanggal = input.nextInt();
        System.out.print("Bulan lahir: ");
        int inputBulan = input.nextInt();
        System.out.print("Tahun lahir: ");
        int inputTahun = input.nextInt();

        String zodiak = "";

//        1. Aries (21 Mei–19 April)
//        2. Taurus (20 April–20 Mei)
//        3. Gemini (21 Mei–20 Juni)
//        4. Cancer (21 Juni–22 Juli)
//        5. Leo (23 Juli–22 Agustus)
//        6. Virgo (23 Agustus–22 September)
//        7. Libra (23 September–22 Oktober)
//        8. Scorpio (24 Oktober–21 November)
//        9. Sagitarius (22 November–21 Desember)
//        10. Capricorn (22 Desember–19 Januari)
//        11. Aquarius (20 Januari–18 Februari)
//        12. Pisces (19 Februari–20 Maret)

        if ((inputBulan == 3 && inputTanggal >= 21) || (inputBulan == 4 && inputTanggal <= 19)) {
            zodiak = "Aries";
        } else if ((inputBulan == 4 && inputTanggal >= 20) || (inputBulan == 5 && inputTanggal <= 20)) {
            zodiak = "Taurus";
        } else if ((inputBulan == 5 && inputTanggal >= 21) || (inputBulan == 6 && inputTanggal <= 20)) {
            zodiak = "Gemini";
        } else if ((inputBulan == 6 && inputTanggal >= 21) || (inputBulan == 7 && inputTanggal <= 21)) {
            zodiak = "Cancer";
        } else if ((inputBulan == 7 && inputTanggal >= 23) || (inputBulan == 8 && inputTanggal <= 22)) {
            zodiak = "Leo";
        } else if ((inputBulan == 8 && inputTanggal >= 23) || (inputBulan == 9 && inputTanggal <= 22)) {
            zodiak = "Virgo";
        } else if ((inputBulan == 9 && inputTanggal >= 23) || (inputBulan == 10 && inputTanggal <= 22)) {
            zodiak = "Libra";
        } else if ((inputBulan == 10 && inputTanggal >= 23) || (inputBulan == 11 && inputTanggal <= 21)) {
            zodiak = "Scorpio";
        } else if ((inputBulan == 11 && inputTanggal >= 22) || (inputBulan == 12 && inputTanggal <= 21)) {
            zodiak = "Sagitarius";
        } else if ((inputBulan == 12 && inputTanggal >= 22) || (inputBulan == 1 && inputTanggal <= 19)) {
            zodiak = "Capricorn";
        } else if ((inputBulan == 1 && inputTanggal >= 20) || (inputBulan == 2 && inputTanggal <= 18)) {
            zodiak = "Aquarius";
        } else if ((inputBulan == 2 && inputTanggal >= 19) || (inputBulan == 3 && inputTanggal <= 20)) {
            zodiak = "Pisces";
        } else {
            zodiak = "Zodiak anda belum terdaftar";
        }

        System.out.println("Nama saya "+inputNama+", zodiak saya "+zodiak);

        input.close();
    }
}

