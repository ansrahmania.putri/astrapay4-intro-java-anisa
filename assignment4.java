//import scanner
import java.util.Scanner;


public class assignment4 {
    //main method
    public static void main(String[] args) {

        //create an object from user
        Scanner input = new Scanner(System.in);

        //print Panjang balok :
        System.out.print("Panjang balok: ");
        //take input panjang from user
        int panjang = input.nextInt();
        //print Lebar balok :
        System.out.print("Lebar balok: ");
        //take input lebar from user
        int lebar = input.nextInt();
        //print Tinggi balok :
        System.out.print("Tinggi balok: ");
        //take input tinggi from user
        int tinggi = input.nextInt();

        ////declare and initialize calculation for volume balok
        int volumeBalok = panjang * lebar * tinggi;

        //print volume balok
        System.out.println("Volume balok adalah: " + volumeBalok);

        System.out.println("-----------------------------");

        //print Nilai pi
        System.out.print("Nilai Pi: ");
        //take input pi from user
        double pi = input.nextDouble();
        //print Nilai p
        System.out.print("Jari-jari: ");
        //take input jari-jari from user
        double r = input.nextDouble();

        ////declare and initialize calculation of volume bola
        double volumeBola = 1.33333 * pi * (r * r * r);

        //print volume bola
        System.out.println("Volume bola: " + volumeBola);

        //closing the scanner object
        input.close();





    }
}
