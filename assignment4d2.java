import java.util.Scanner;

public class assignment4d2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Tahun lahir: ");
        int tahunLahir = input.nextInt();
        System.out.print("Tahun sekarang: ");
        int tahunSekarang = input.nextInt();

        if (tahunSekarang > tahunLahir){
            int umur = tahunSekarang - tahunLahir;
            for ( int i = umur; i >= 1 ; --i ) {
                System.out.println(umur + " tahun pada tahun " + tahunSekarang);
                umur--;
                tahunSekarang--;
            }
        } else {
            System.out.println("Tahun yang anda masukkan tidak valid");
        }

        input.close();



    }
}
