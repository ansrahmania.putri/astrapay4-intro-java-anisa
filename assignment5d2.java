import java.util.Scanner;

public class assignment5d2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choice = 0;

        do {
            System.out.println("Menu");
            System.out.println("1. Menu 1");
            System.out.println("2. Menu 2");
            System.out.println("3. Menu 3");
            System.out.println("4. Exit");
            System.out.println("Input nomor (1/2/3/4)");
            choice = input.nextInt();

            if (choice <= 3) {
                System.out.println("Cetak Menu "+ choice);
            } else if (choice == 4){
                break;
            } else {
                System.out.println("Menu tidak tersedia");
            }
        } while (choice > 0);

        input.close();



    }
}
