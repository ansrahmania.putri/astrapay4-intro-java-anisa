import java.util.Scanner;

public class assignment6d2 {
    public static int volumeBalok (int p, int l, int t){
        return p * l * t;
    }
    public static double volumeBola (double pi, double r) {
        return 1.33333 * pi * (r * r * r);
    }
    public static int umur (int tahunLahir, int tahunSekarang) {
        return tahunSekarang - tahunLahir;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choice = 0;

        do {
            System.out.println("Menu");
            System.out.println("1. Volume Balok");
            System.out.println("2. Volume Bola");
            System.out.println("3. Hitung Umur");
            System.out.println("4. EXIT");
            System.out.println("Input nomor (1/2/3/4)");
            choice = input.nextInt();

            if (choice == 1) {
                System.out.print("Panjang balok: ");
                int panjang = input.nextInt();
                System.out.print("Lebar balok: ");
                int lebar = input.nextInt();
                System.out.print("Tinggi balok: ");
                int tinggi = input.nextInt();

                System.out.println("Volume balok adalah : " + volumeBalok(panjang, lebar, tinggi));

            } else if (choice == 2) {
                System.out.print("Nilai Pi: ");
                double pi = input.nextDouble();
                System.out.print("Jari-jari: ");
                double r = input.nextDouble();

                System.out.println("Volume bola adalah : " + volumeBola(pi, r));

            } else if (choice == 3) {
                System.out.println("Tahun lahir: ");
                int tahunLahir = input.nextInt();
                System.out.println("Tahun sekarang: ");
                int tahunSekarang = input.nextInt();

                if (tahunSekarang > tahunLahir) {
                    System.out.println("Umur mu sekarang adalah : " + umur(tahunLahir, tahunSekarang));
                } else {
                    System.out.println("Tahun sekarang harus lebih besar dari tahun lahirmu");
                }
            } else if (choice == 4) {
                break;
            } else {
                System.out.println("Pilihan mu tidak ada dimenu");
            }
        } while (true);

        input.close();

    }
}

